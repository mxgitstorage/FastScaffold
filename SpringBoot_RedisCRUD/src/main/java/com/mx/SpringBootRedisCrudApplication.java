package com.mx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author muxin
 */
@SpringBootApplication
@MapperScan("com.mx.dao")
public class SpringBootRedisCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRedisCrudApplication.class, args);
    }

}
