package com.mx.cache;

import com.mx.util.BeanFactoryUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/4/10 下午1:53
 * @package com.cache
 */
@Slf4j
public class MyBatisCache implements Cache {

    private final String id;

    public MyBatisCache(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void putObject(Object key, Object value) {
        getRedisTemplate().opsForHash().put(id.toString(),key.toString(),value);
        log.info("写入缓存");
        log.info("key = "+key.toString());
        log.info("value = "+value.toString());
    }

    @Override
    public Object getObject(Object key) {
        log.info("获取缓存");
        log.info("key = "+key.toString());
        return getRedisTemplate().opsForHash().get(id.toString(),key.toString());
    }

    @Override
    public Object removeObject(Object key) {
        log.info("清空指定key缓存");
        log.info("key = "+key.toString());
        return getRedisTemplate().opsForHash().delete(id.toString(),key.toString());
    }

    @Override
    public void clear() {
        getRedisTemplate().delete(id.toString());
        log.info("清空缓存");
        log.info("id = "+id.toString());
    }

    @Override
    public int getSize() {
        return 0;
    }

    private RedisTemplate getRedisTemplate(){
        RedisTemplate redisTemplate= (RedisTemplate) BeanFactoryUtil.getBean("redisTemplate");
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        return redisTemplate;
    }
}
