package com.mx.controller;

import com.mx.entity.User;
import com.mx.service.impl.UserOperationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/4/10 下午3:06
 * @package com.mx.controller
 */
@RestController
public class UserOperationController {

    @Autowired
    private UserOperationServiceImpl userOperationService;

    @GetMapping("/show")
    public String showAllUserController(){
        List<User> users = userOperationService.showAllUser();
        return users.toString();
    }
}
