package com.mx.dao;

import com.mx.entity.User;

import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/4/10 下午1:41
 * @package com.mx.dao
 */
public interface UserOperationMapper {
    List<User> showAllUser();
    User showUserById(String id);
    int deleteUserById(String id);
    int updateUserById(User user);
    int addUser(User user);
}
