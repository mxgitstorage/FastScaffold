package com.mx.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/4/10 下午1:37
 * @package com.mx.entity
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private String user_id;
    private String user_name;
    private String user_address;
}
