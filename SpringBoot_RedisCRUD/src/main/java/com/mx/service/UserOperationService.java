package com.mx.service;

import com.mx.entity.User;

import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/4/10 下午1:44
 * @package com.mx.service
 */
public interface UserOperationService {
    List<User> showAllUser();
    User showUserById(String id);
    int deleteUserById(String id);
    int updateUserById(User user);
    int addUser(User user);
}
