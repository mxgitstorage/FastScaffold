package com.mx.service.impl;

import com.mx.dao.UserOperationMapper;
import com.mx.entity.User;
import com.mx.service.UserOperationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/4/10 下午1:45
 * @package com.mx.service.impl
 */
@Service
@Transactional
@Slf4j
public class UserOperationServiceImpl implements UserOperationService {

    @Autowired
    private UserOperationMapper userOperationMapper;

    @Override
    public List<User> showAllUser() {
        return userOperationMapper.showAllUser();
    }

    @Override
    public User showUserById(String id) {
        return userOperationMapper.showUserById(id);
    }

    @Override
    public int deleteUserById(String id) {
        if(userOperationMapper.deleteUserById(id)!=1){
            return 0;
        }
        return 1;
    }

    @Override
    public int updateUserById(User user) {
        if(userOperationMapper.updateUserById(user)!=1){
            return 0;
        }
        return 1;
    }

    @Override
    public int addUser(User user) {
        if(userOperationMapper.addUser(user)!=1){
            return 0;
        }
        return 1;
    }
}
