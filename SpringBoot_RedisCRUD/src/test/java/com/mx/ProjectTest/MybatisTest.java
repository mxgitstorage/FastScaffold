package com.mx.ProjectTest;

import com.mx.entity.User;
import com.mx.service.impl.UserOperationServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/4/10 下午2:14
 * @package com.mx.ProjectTest
 */
@SpringBootTest
@Slf4j
public class MybatisTest {
    @Autowired
    private UserOperationServiceImpl userOperationService;

    @Test
    public void showAllUserTest(){
        userOperationService.showAllUser().forEach(list-> System.out.println("list = " + list));
    }

    @Test
    public void showUserByIdTest(){
        User user = userOperationService.showUserById("1");
        log.info("结果为:"+user.toString());
    }

    @Test
    public void updateUserByIdTest(){
        User user=new User("1","你爹","皇家海岸");
        int i = userOperationService.updateUserById(user);
        log.info("修改状态 = "+i);
    }

    @Test
    public void addUserTest(){
        User user=new User("3","1111","22234");
        int i = userOperationService.addUser(user);
        log.info("添加状态 = "+i);
    }

    @Test
    public void deleteUserByIdTest(){
        int i = userOperationService.deleteUserById("2");
        log.info("删除状态 = "+i);
    }
}
