package com.mx.controller;

import com.mx.pojo.CrudEntity;
import com.mx.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;

/**
 * @author MX
 * @create By MX-Desktop 2022/2/18 0:08
 */
@Controller
@RequestMapping("/Crud")
public class CrudServiceController {

    @Autowired
    private CrudService crudService;

    @RequestMapping("/goAdd")
    public String goAdd(){
        return "addPage";
    }

    @RequestMapping("/add")
    public String addEntity(CrudEntity crudEntity){
        this.goAdd();
        int status=crudService.addEntity(crudEntity);
        if (status!=1){
            return "error";
        }else {
            return "redirect:show";
        }
    }

    @RequestMapping("/goUpdate")
    public String goUpdate(){
        return "updatePage";
    }

    @RequestMapping("/update")
    public String updateEntity(CrudEntity crudEntity){
        int status=crudService.updateEntity(crudEntity);
        if (status!=1){
            return "error";
        }else {
            return "redirect:show";
        }
    }

    @RequestMapping("/show")
    public String showAllEntity(Model model){
        ArrayList<CrudEntity> list=crudService.showAllEntity();
        model.addAttribute("entityList",list);
        return "showAll";
    }

    @RequestMapping("find")
    public String showEntityById(int id,Model model){
        ArrayList<CrudEntity> list=crudService.findEntityById(id);
        model.addAttribute("list",list);
        return "showAll";
    }

    @RequestMapping("/delete")
    public String deleteEntity(int id){
        int status=crudService.deleteEntityById(id);
        if (status!=1){
            return "error";
        }else {
            return "redirect:show";
        }
    }

}
