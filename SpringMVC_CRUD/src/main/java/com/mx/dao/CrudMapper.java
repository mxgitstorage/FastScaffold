package com.mx.dao;

import com.mx.pojo.CrudEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MX
 * @create By MX-Desktop 2022/2/17 23:18
 */
public interface CrudMapper {
    public ArrayList<CrudEntity> findEntityById(int id);
    public ArrayList<CrudEntity> showAllEntity();
    public int addEntity(CrudEntity crudEntity);
    public int updateEntity(CrudEntity crudEntity);
    public int deleteEntityById(int id);
}
