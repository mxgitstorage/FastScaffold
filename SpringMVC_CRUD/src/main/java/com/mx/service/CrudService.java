package com.mx.service;

import com.mx.pojo.CrudEntity;

import java.util.ArrayList;

/**
 * @author MX
 * @create By MX-Desktop 2022/2/17 23:58
 */
public interface CrudService {
    public ArrayList<CrudEntity> findEntityById(int id);
    public ArrayList<CrudEntity> showAllEntity();
    public int addEntity(CrudEntity crudEntity);
    public int updateEntity(CrudEntity crudEntity);
    public int deleteEntityById(int id);
}
