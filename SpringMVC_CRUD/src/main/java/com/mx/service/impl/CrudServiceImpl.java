package com.mx.service.impl;

import com.mx.dao.CrudMapper;
import com.mx.pojo.CrudEntity;
import com.mx.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author MX
 * @create By MX-Desktop 2022/2/17 23:58
 */
@Service
public class CrudServiceImpl implements CrudService {

    @Autowired
    private CrudMapper crudMapper;

    @Override
    public ArrayList<CrudEntity> findEntityById(int id) {
        return crudMapper.findEntityById(id);
    }

    @Override
    public ArrayList<CrudEntity> showAllEntity() {
        return crudMapper.showAllEntity();
    }

    @Override
    public int addEntity(CrudEntity crudEntity) {
        return crudMapper.addEntity(crudEntity);
    }

    @Override
    public int updateEntity(CrudEntity crudEntity) {
        return crudMapper.updateEntity(crudEntity);
    }

    @Override
    public int deleteEntityById(int id) {
        return crudMapper.deleteEntityById(id);
    }
}
