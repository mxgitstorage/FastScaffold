<%--
  Created by IntelliJ IDEA.
  User: MX
  Date: 2022/2/18
  Time: 21:40
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>查看所有用户</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<div class="container">

    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>显示所有</small>
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 column">
            <a class="btn btn-primary" href="#">退出</a>
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/Crud/add">添加</a>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Address</th>
                </tr>
                </thead>
                <c:forEach var="list" items="${entityList}">
                    <tbody>
                    <tr>
                        <td>${list.id}</td>
                        <td>${list.name}</td>
                        <td>${list.address}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/Crud/update">修改</a>
                            |
                            <a href="${pageContext.request.contextPath}/Crud/delete?id=${list.id}">删除</a>
                        </td>
                    </tr>
                    </tbody>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
</body>
</html>
