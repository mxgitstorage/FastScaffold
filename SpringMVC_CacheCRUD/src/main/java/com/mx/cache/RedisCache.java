package com.mx.cache;

import com.mx.util.RedisUtil;
import org.apache.ibatis.cache.Cache;

import java.util.ArrayList;
import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/5/4 下午3:14
 * @package com.mx.cache
 */
public class RedisCache implements Cache {

    private final String id;

    public RedisCache(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void putObject(Object key, Object value) {
        RedisUtil.getJedis().hset(id,key.toString(),value.toString());
        RedisUtil.getJedis().close();
    }

    @Override
    public Object getObject(Object key) {
        List list=new ArrayList();
        String result=RedisUtil.getJedis().hget(id,key.toString());
        RedisUtil.getJedis().close();
        if(result!=null)
        {
            list.add(result);
            return list;
        }
        return null;
    }

    @Override
    public Object removeObject(Object key) {
        RedisUtil.getJedis().flushDB();
        RedisUtil.getJedis().close();
        return null;
    }

    @Override
    public void clear() {
        RedisUtil.getJedis().flushDB();
        RedisUtil.getJedis().close();
    }

    @Override
    public int getSize() {
        return 0;
    }
}
