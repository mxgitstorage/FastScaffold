package com.mx.controller;

import com.mx.entity.Entity;
import com.mx.service.impl.EntityOpServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/5/5 下午3:42
 * @package com.mx.controller
 */
@RestController
public class MenuController {

    @Autowired
    private EntityOpServiceImpl entityOpService;

    @RequestMapping(method = RequestMethod.GET,value = "/show")
    public String showAllEntityPage(HttpServletRequest request){
        List list=entityOpService.showAll();
        request.getSession().setAttribute("list",list);
        return String.valueOf(list);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/show/{id}")
    public String showAllById(@PathVariable String id){
        List list=entityOpService.showEntityById(id);
        return String.valueOf(list);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/show")
    public String addEntity(@RequestBody Entity entity){
        int status= entityOpService.addEntity(entity);
        return String.valueOf(status);
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/show")
    public String updateEntity(@RequestBody Entity entity){
        int status= entityOpService.updateEntityById(entity);
        return String.valueOf(status);
    }


    @RequestMapping(method = RequestMethod.DELETE,value = "/show/{id}")
    public String deleteEntityById(@PathVariable String id){
        int status= entityOpService.deleteEntityById(id);
        return String.valueOf(status);
    }

    @RequestMapping(method = RequestMethod.OPTIONS,value = "/show/{b}")
    public String deleteAll(@PathVariable String b){
        int status= entityOpService.deleteAll(Boolean.valueOf(b));
        return String.valueOf(status);
    }
}
