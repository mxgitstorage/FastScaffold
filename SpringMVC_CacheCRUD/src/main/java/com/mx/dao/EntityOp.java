package com.mx.dao;

import com.mx.entity.Entity;

import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/5/2 下午3:16
 * @package com.mx.dao
 */
public interface EntityOp {
    List<Entity> showAll();
    @Deprecated
    String showAllString();
    List<Entity> showEntityById(String id);
    int addEntity(Entity entity);
    int deleteEntityById(String id);
    int updateEntityById(Entity entity);
    int deleteAll(Boolean status);
}
