package com.mx.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/5/2 下午3:16
 * @package com.mx.entity
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Entity implements Serializable {
    private int entity_id;
    private String entity_name;
    private String entity_attribute;
}
