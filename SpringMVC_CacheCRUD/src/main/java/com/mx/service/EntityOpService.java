package com.mx.service;

import com.mx.entity.Entity;

import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/5/2 下午4:50
 * @package com.mx.service
 */
public interface EntityOpService {
    List<Entity> showAll();
    @Deprecated
    String showAllString();
    List<Entity> showEntityById(String id);
    int addEntity(Entity entity);
    int deleteEntityById(String id);
    int updateEntityById(Entity entity);
    int deleteAll(Boolean status);
}
