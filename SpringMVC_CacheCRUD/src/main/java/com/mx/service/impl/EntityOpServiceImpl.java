package com.mx.service.impl;

import com.mx.dao.EntityOp;
import com.mx.entity.Entity;
import com.mx.service.EntityOpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/5/2 下午4:51
 * @package com.mx.service.impl
 */
@Service
public class EntityOpServiceImpl implements EntityOpService {

    @Autowired
    private EntityOp entityOp;

    @Override
    public List<Entity> showAll() {
        return entityOp.showAll();
    }

    @Override
    public String showAllString() {
        return entityOp.showAllString();
    }

    @Override
    public List<Entity> showEntityById(String id) {
        return entityOp.showEntityById(id);
    }

    @Override
    public int addEntity(Entity entity) {
        if(entityOp.addEntity(entity)!=1){
            return 0;
        }
        return 1;
    }

    @Override
    public int deleteEntityById(String id) {
        if(entityOp.deleteEntityById(id)!=1){
            return 0;
        }
        return 1;
    }

    @Override
    public int updateEntityById(Entity entity) {
        if(entityOp.updateEntityById(entity)!=1){
            return 0;
        }
        return 1;
    }

    @Override
    public int deleteAll(Boolean status) {
        if(!status){
            return 0;
        }
        if(entityOp.deleteAll(status)!=1){
            return 0;
        }else{
            return 1;
        }
    }
}
