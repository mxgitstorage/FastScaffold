package com.mx.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/5/2 下午3:24
 * @package com.mx.util
 */
public class BeanUtil implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }

    public Object getBeanUtil(String beanName) {
        return applicationContext.getBean(beanName);
    }
}
