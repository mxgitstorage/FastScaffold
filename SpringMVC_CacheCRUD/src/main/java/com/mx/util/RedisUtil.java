package com.mx.util;

import redis.clients.jedis.Jedis;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/5/2 下午3:25
 * @package com.mx.util
 */
public class RedisUtil {

    private static Jedis jedis;

    static {
        jedis=new Jedis("192.168.199.205",6379);
    }

    public static Jedis getJedis(){
        return jedis;
    }
}
