package com.mx.test;

import com.mx.entity.Entity;
import com.mx.service.EntityOpService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author muxin By Ubuntu20.04
 * @time 2022/5/4 下午2:10
 * @package com.mx.test
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = {"classpath:spring/application.xml"})
@Slf4j
public class MybatisTest {
    //非springboot项目需要手动加载spring容器和加载配置文件。
    @Autowired
    private EntityOpService entityOpService;

    @Test
    public void showAllTest() {
        List list= entityOpService.showAll();
        System.out.println("list = " + list);
    }

    @Test
    public void showEntityById(){
        List list=entityOpService.showEntityById("3");
        System.out.println(list);
    }

    @Test
    public void addEntityTest(){
        System.out.println(entityOpService.addEntity(new Entity(0,"MXE","people")));
    }

    @Test
    public void updateEntityByIdTest(){
        System.out.println(entityOpService.updateEntityById(new Entity(1,"MXupdate","Chinese Communist Party members")));
    }

    @Test
    public void deleteEntityByIdTest(){
        System.out.println(entityOpService.deleteEntityById("3"));
    }
}
